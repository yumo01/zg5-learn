package main

import (
	"gitee.com/yumo01/zg5-learn/began"
	_ "gitee.com/yumo01/zg5-learn/initia"
	"gitee.com/yumo01/zg5-learn/logic"
	"google.golang.org/grpc"
)

func main() {
	began.ServerStart(&grpc.Server{}, func(s *grpc.Server) {
		logic.RegisterUsersServer(s)
	})
}
