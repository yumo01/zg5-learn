package logic

import (
	"context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitee.com/yumo01/zg5-learn/pb/users"
)

type UsersServer struct {
	users.UnimplementedUsersServer
}

// UnimplementedUsersServer must be embedded to have forward compatible implementations.

func (UsersServer) Ping(context.Context, *users.Request) (*users.Response, error) {

	return &users.Response{
		Pong: "pong",
	}, status.Errorf(codes.OK, "测试成功")
}
func (UsersServer) Login(context.Context, *users.User) (*users.Response, error) {
	return nil, status.Errorf(codes.OK, "")
}

func RegisterUsersServer(s grpc.ServiceRegistrar) {
	s.RegisterService(&users.Users_ServiceDesc, UsersServer{})
}
