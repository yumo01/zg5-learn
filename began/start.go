package began

import (
	"gitee.com/yumo01/zg5-learn/global"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/health"
	"google.golang.org/grpc/health/grpc_health_v1"
	"net"
	"strconv"
)

func ServerStart(s *grpc.Server, f func(s *grpc.Server)) {
	lis, err := net.Listen("tcp", "0.0.0.0:"+strconv.Itoa(global.Conf.Port))
	if err != nil {
		zap.S().Fatal("监听端口失败", err)
	}
	zap.S().Info("开始监听端口:", global.Conf.Port)

	s = grpc.NewServer()
	grpc_health_v1.RegisterHealthServer(s, health.NewServer())

	f(s)
	zap.S().Info("服务正在启动。。。")
	err = s.Serve(lis)
	if err != nil {
		zap.S().Fatal("服务启动失败", err)
	}
	zap.S().Info("服务正在关闭。。。")

}
