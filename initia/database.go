package initia

import (
	"gitee.com/yumo01/zg5-learn/global"
	"go.uber.org/zap"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func Mysql() {
	var err error
	global.MysqlDB, err = gorm.Open(mysql.Open(global.Conf.Mysql.Root+":"+global.Conf.Mysql.Password+"@tcp("+global.Conf.Mysql.Host+":"+global.Conf.Mysql.Port+")/"+global.Conf.Mysql.Database), &gorm.Config{})
	if err != nil {
		zap.S().Panic("连接数据库失败", err)
	}
	zap.S().Info("数据库连接成功")
}
