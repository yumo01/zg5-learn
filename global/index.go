package global

import (
	"gorm.io/gorm"
)

type ServerConf struct {
	Name   string     `mapstructure:"serverName"`
	Port   int        `yaml:"port"`
	Nacos  NacosConf  `mapstructure:"nacos"`
	Mysql  MysqlConf  `yaml:"mysql"`
	Consul ConsulConf `mapstructure:"consul"`
}

type NacosConf struct {
	ServerConfig struct {
		IpAddr string `mapstructure:"IpAddr"`
		Port   uint64 `mapstructure:"Port"`
	}
	ClientConfig struct {
		NamespaceId string `mapstructure:"NamespaceId"`
	}
	ConfigParam struct {
		DataId string `mapstructure:"DataId"`
		Group  string `mapstructure:"Group"`
	}
}

type MysqlConf struct {
	Root     string `yaml:"root"`
	Password string `yaml:"password"`
	Host     string `yaml:"host"`
	Port     string `yaml:"port"`
	Database string `yaml:"database"`
}

type ConsulConf struct {
	Host string `yaml:"host"`
	Port int    `yaml:"port"`
}

var (
	Conf    ServerConf
	MysqlDB *gorm.DB
)
